import cmath
import math

def quadratic(a, b, c):
    """
    solves a quadratic function given a, b, c
    """
    det =cmath.sqrt((b)**2-4*a*c)
    x1 = (-b+det)/(2*a)
    x2 = (-b-det)/(2*a)

    return(x1, x2)

a=float(input('a = : '))
b=float(input('b = : '))
c=float(input('c = : '))

print(quadratic(a,b,c))
# sol = quadratic(a,b,c)
# print(sol)
